
# ThingsBoard Azure Microservices Installation

This document is a step-by-step guide to install ThingsBoard Community Edition in the cloud using Azure Kubernetes Service (AKS). The deployment is made with microservices using Azure Database for PostgreSQL and Azure Cache for Redis. This guide is strongly based in [ThingsBoard official documentation](https://thingsboard.io/docs/user-guide/install/cluster/azure-microservices-setup/), but it has some additional steps and information that are essential to a successful installation. Cassandra database is not used in thid guide.

## Prerequisites

The installation was made in the following scenario:

- Ubuntu 22.04 LTS VM running on Windows 11 **(important: the installation failed in Ubuntu 20.04 and 18.04)**
- Microsoft Azure account with pay-as-you-go subscription. To avoid unexpected errors, use a fresh Azure account.

## Step 1: Installing the necessary tools

First, we have to install **kubectl** and **az** command-line tools. 

### kubectl

To install kubectl, use [this guide](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/). Be aware of the kubectl version you will install, you must use a kubectl version that is within one minor version difference of your cluster (e.g., if your cluster version is 1.23, it's only able to communnicate with 1.24, 1.23 and 1.22 kubectl version). The kubectl version used in this guide is 1.23, because our cluster version is 1.22.

### az

To install az, use [this guide](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt#option-1-install-with-one-command). You will find two options of installation. For simplicity's sake, use option 1. After installation is complete, use `az login` and login to your Azure account in your browser.

## Step 2: Clone ThingsBoard CE K8S scripts repository

Use the following commands to clone ThingsBoard CE K8S scripts repository and navigate to the correct folder.

```
git clone -b release-3.4 https://github.com/thingsboard/thingsboard-ce-k8s.git
cd thingsboard-ce-k8s/azure/microservices
```

## Step 3: Define the environment variables 

We will use some variables repeatedly along the installation, so it's a good idea to define some environment variables:

```
export AKS_RESOURCE_GROUP=ThingsBoardResources
export AKS_LOCATION=eastus
export AKS_GATEWAY=tb-gateway
export TB_CLUSTER_NAME=tb-cluster
export TB_DATABASE_NAME=isensitb-db
export TB_REDIS_NAME=isensitb-redis
echo "You variables ready to create resource group $AKS_RESOURCE_GROUP in location $AKS_LOCATION 
and cluster in it $TB_CLUSTER_NAME with database $TB_DATABASE_NAME"
```

You should see the message 'You variables ready to create resource group ThingsBoardResources in location eastus and cluster in it tb-cluster with database isensitb-db' printed as a result. You can freely modify your variables, execpt for AKS_LOCATION, which is the location where our Azure Resource Group will store its metadata. To see all the avaiable locations, run `az account list-locations`. You must choose one that has 'regionCategory' set to 'Recommended' and then replace 'eastus' by the 'name' value of the location.

## Step 4: Configure and create AKS cluster

First, create an Azure Resource Group named ThingsBoardResources with location set to eastus (East US):

```
az group create --name $AKS_RESOURCE_GROUP --location $AKS_LOCATION
```

To learn more about **az group** command, check [this link](https://docs.microsoft.com/en-us/cli/azure/group?view=azure-cli-latest).

Now, we can create our AKS cluster (this might take some time):

```
az aks create --resource-group $AKS_RESOURCE_GROUP \
    --name $TB_CLUSTER_NAME \
    --generate-ssh-keys \
    --enable-addons ingress-appgw \
    --appgw-name $AKS_GATEWAY \
    --appgw-subnet-cidr "10.225.0.0/16" \
    --node-count 2
```

- Obs. 1: ThingsBoard official documentation uses `-appgw-subnet-cidr "10.2.0.0/16"`, which results in the following error:

*(IngressAppGwAddonConfigInvalidSubnetCIDRNotContainedWithinVirtualNetwork) Subnet Prefix '10.2.0.0/16' specified for IngressApplicationGateway addon is not contained within the AKS Agent Pool's Virtual Network address prefixes '\[10.224.0.0/12\]'.
Code: IngressAppGwAddonConfigInvalidSubnetCIDRNotContainedWithinVirtualNetwork
Message: Subnet Prefix '10.2.0.0/16' specified for IngressApplicationGateway addon is not contained within the AKS Agent Pool's Virtual Network address prefixes '\[10.224.0.0/12\]'.
Target: AddonProfiles.IngressApplicationGateway*

- Obs. 2: ThingsBoard official documentation uses `--node-count 3` and an additional argument `--node-vm-size Standard_DS3_v2`, which results in the following error (after fixing the previous one):

*(QuotaExceeded) Provisioning of resource(s) for container service tb-cluster in resource group ThingsBoardResources failed. Message: Operation could not be completed as it results in exceeding approved Total Regional Cores quota. Additional details - Deployment Model: Resource Manager, Location: eastus, Current Limit: 10, Current Usage: 0, Additional Required: 12, (Minimum) New Limit Required: 12. Submit a request for Quota increase at https://aka.ms/ProdportalCRP/#blade/Microsoft_Azure_Capacity/UsageAndQuota.ReactView/Parameters/%7B%22subscriptionId%22:%2238b47943-b011-4116-afca-f58a35e77a2c%22,%22command%22:%22openQuotaApprovalBlade%22,%22quotas%22:\[%7B%22location%22:%22eastus%22,%22providerId%22:%22Microsoft.Compute%22,%22resourceName%22:%22cores%22,%22quotaRequest%22:%7B%22properties%22:%7B%22limit%22:12,%22unit%22:%22Count%22,%22name%22:%7B%22value%22:%22cores%22%7D%7D%7D%7D\]%7D by specifying parameters listed in the ‘Details’ section for deployment to succeed. Please read more about quota limits at https://docs.microsoft.com/en-us/azure/azure-supportability/regional-quota-requests. Details:
Code: QuotaExceeded
Message: Provisioning of resource(s) for container service tb-cluster in resource group ThingsBoardResources failed. Message: Operation could not be completed as it results in exceeding approved Total Regional Cores quota. Additional details - Deployment Model: Resource Manager, Location: eastus, Current Limit: 10, Current Usage: 0, Additional Required: 12, (Minimum) New Limit Required: 12. Submit a request for Quota increase at https://aka.ms/ProdportalCRP/#blade/Microsoft_Azure_Capacity/UsageAndQuota.ReactView/Parameters/%7B%22subscriptionId%22:%2238b47943-b011-4116-afca-f58a35e77a2c%22,%22command%22:%22openQuotaApprovalBlade%22,%22quotas%22:\[%7B%22location%22:%22eastus%22,%22providerId%22:%22Microsoft.Compute%22,%22resourceName%22:%22cores%22,%22quotaRequest%22:%7B%22properties%22:%7B%22limit%22:12,%22unit%22:%22Count%22,%22name%22:%7B%22value%22:%22cores%22%7D%7D%7D%7D\]%7D by specifying parameters listed in the ‘Details’ section for deployment to succeed. Please read more about quota limits at https://docs.microsoft.com/en-us/azure/azure-supportability/regional-quota-requests. Details:*

- Obs. 3: If you receive 'Specified server name is already used.' as response, change TB_DATABASE_NAME value.

You can learn more about **az aks create** [here](https://docs.microsoft.com/en-us/cli/azure/aks?view=azure-cli-latest#az_aks_create). Alternatively, to create a custom cluster, use [this guide](https://docs.microsoft.com/en-us/azure/aks/learn/quick-kubernetes-deploy-portal?tabs=azure-cli).

## Step 5: Update kubectl context

Connect your cluster to kubectl:

```
az aks get-credentials --resource-group $AKS_RESOURCE_GROUP --name $TB_CLUSTER_NAME
```

Make sure it worked by running `kubectl get nodes`, you should see a list of the nodes you created in step 4.

## Step 6: Create Azure Database for PostgreSQL server

Create the database server using the following az command, replacing 'POSTGRES_USER' and 'POSTGRES_PASS' by the desired username and password, respectively. Keep in mind that they can't be changed after they're defined.

```
az postgres flexible-server create --location $AKS_LOCATION \
    --resource-group $AKS_RESOURCE_GROUP \
    --name $TB_DATABASE_NAME \
    --admin-user POSTGRESS_USER --admin-password POSTGRESS_PASS \
    --public-access 0.0.0.0 --storage-size 32 \
    --version 12 -d thingsboard
```

This might also take some time. 
See [this documentation](https://docs.microsoft.com/en-us/cli/azure/postgres/flexible-server?view=azure-cli-latest#az_postgres_flexible_server_create) for more information about the **az postgres flexible-server create** command. Alternatively, follow [this guide](https://docs.microsoft.com/en-us/azure/postgresql/flexible-server/quickstart-create-server-portal) to create the server, bearing in mind the following points:
- Your Azure Database for PostgreSQL version must be latest 12.x
- Your Azure Database for PostgreSQL instance must be accessible from the ThingsBoard cluster
- You must use 'thingsboard' as initial database name
- Use 'High availability' enabled in order to enable a lot of useful settings by default
After the server is created, you will receive a response with a 'host' parameter. Copy it, run `nano tb-node-db-configmap.yml` and replace YOUR_AZURE_POSTGRES_ENDPOINT_URL with the 'host' value. Still in tb-node-db-configmap.yml, replace YOUR_AZURE_POSTGRES_USER and YOUR_AZURE_POSTGRES_PASSWORD with the username and password you used, respectively.

## Step 7: Configure Azure Cache dor Redis

Use the following command to create Azure Cache for Redis:

```
az redis create --name $TB_REDIS_NAME --location $AKS_LOCATION \
    --resource-group $AKS_RESOURCE_GROUP --sku Basic --vm-size c0 \
    --enable-non-ssl-port
```

This will take some time. See [this](https://docs.microsoft.com/en-us/cli/azure/redis?view=azure-cli-latest#az_redis_create) for further information on **az redis create**. See [this guide](https://docs.microsoft.com/en-us/azure/azure-cache-for-redis/quickstart-create-redis) to configure the Redis cache for yourself.
After it's done, you will receive a response with a 'hostName' parameter. Copy it, run `nano tb-redis-configmap.yml` and replace YOUR_REDIS_ENDPOINT_URL_WITHOUT_PORT for the 'hostName' value. Now, run the following command:

```
az redis list-keys --name $TB_REDIS_NAME \
    --resource-group $AKS_RESOURCE_GROUP
```

Copy the 'primaryKey' value, run `nano tb-redis-configmap.yml` again and replace YOUR_REDIS_PASS with it.

- Obs. 1: If you receive the error below configuring the Redis cache, change TB_REDIS_NAME value.

*(NameNotAvailable) An error occured when trying to reserve the DNS name for the cache instance. This may be a temporary issue if a cache instance of this name was recently deleted. Please choose a different name or try again later.
RequestID=aa03606a-574e-4ddc-901d-2f0e35b06e8f
Code: NameNotAvailable
Message: An error occured when trying to reserve the DNS name for the cache instance. This may be a temporary issue if a cache instance of this name was recently deleted. Please choose a different name or try again later.
RequestID=aa03606a-574e-4ddc-901d-2f0e35b06e8f*

## Step 8: Installation

Execute the following command to install ThingsBoard:

```
./k8s-install-tb.sh --loadDemo
```

If you get 'Permission denied', run `chmod +x k8s-install-tb.sh` and try again.
The **loadDemo** parameter loads additional demo data to ThingsBoard. Within some minutes, you should see the 'Installation finished successfully!' message printed.

## Step 9: Starting

Execute the following command to deploy ThingsBoard resources and services:

```
./k8s-deploy-resources.sh
```

If you get 'Permission denied', run `chmod +x k8s-deploy-resources.sh` and try again.
After a few minutes, run `kubectl get pods`, if the **tb-node-0** is in **READY** state, it meand that everything worked fine.
To deploy the transport microservices (optional), run `kubectl apply -f transports/yml-transport-file.yml`, replacing 'yml-transport-file' by the file of the transport microservice you want to deploy, for example:

```
kubectl apply -f transports/tb-http-transport.yml
```

To see avaiable transport files:

```
cd transports
ls
```

## Step 10: Configure HTTP load balancer

Execute the following command to deploy the HTTP load balancer:

```
kubectl apply -f receipts/http-load-balancer.yml
```

To check the load balancer status, run `kubectl get ingress`. If everything went fine, you can now copy the load balancer address, paste it in your browser and access ThingsBoard web UI, congratulations! The default system admnistrator user and password are 'sysadmin@thingsboard.org' and 'sysadmin', respectively.
